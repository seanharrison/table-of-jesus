# The Table of Jesus

The table of Jesus is a simple gathering to remember the life, death, and resurrection of Jesus. Jesus invites his followers to meet with him regularly and share a simple meal together to remember him (1 Cor. 11:23-26). He invites all people who trust him, who believe his good news and follow him as Savior and Lord, to remember and worship him in this way. He calls us to gather for worship, fellowship, prayer, encouragement, and instruction (Acts 2:42; 1 Cor. 14:26). He promises that when we gather, he himself is among us (Matt. 18:20).

This practice of meeting together at the table of Jesus is a simple one, but it might be unfamiliar even to people who have been following Jesus for many years, because it is unlike a typical church service that is led from the front and scripted in advance. So the rest of this document seeks to answer questions and make it easy to understand and put into practice.

## How does it work?

A group of Christian believers meets in a home or other gathering place. Some bread and wine/juice are set on a table to remind us of the life, death, and resurrection of Jesus as the central event of history.

At the beginning of the meeting, everyone becomes quiet and is encouraged to have an attitude of anticipation: The purpose of the meeting is to remember Jesus, to listen to him and to each other, and to speak to him and to each other. Someone will open the meeting with one or more expressions of worship: They will read a passage of Scripture, verbally reflect on the meaning of that passage, say a prayer, invite the gathering to sing a song of worship together, sing a song alone or with one or more others, play instrumental music, or express worship in some other way. Then someone else will offer one or more expressions of worship. Each one participates — verbally, non-verbally, or silently — as they sense the Lord leading them.

Near the end of the meeting, the participants share the Lord’s Supper — a simple meal of bread and wine/juice. Someone will thank God for the bread, representing the body of Christ, and it will be passed around. Someone will thank God for the wine/juice, representing the blood of Christ, and it will be shared. This is his feast, which he has given to his people, and by which we remember him. As we eat and drink, we remember who he is and what he has done, and we thank him.

## Who can participate?

Everyone is welcome at the table of Jesus. Those who are Christian believers, who follow Jesus and trust him as Savior and Lord, are welcome to participate fully. 

People who are not Christian believers are also welcome to attend and observe. (Christian believers have no secret rituals and nothing to hide, and want to invite everyone into fellowship with Jesus.) But those who are not Christian believers can’t really participate in this worship of Jesus, because they don’t yet follow him or truly worship him. Until they do, we ask that they listen and observe, and not speak or partake of the bread and wine/juice. Most people are respectful and understand that.

## How does someone know when it is their turn?

How do you know when it’s your turn to talk at the dinner table? The table of Jesus is a more solemn occasion than suppertime, but the principle is similar: If God is your Father and other Christians are your brothers and sisters, then you are in conversation with your family, and you will learn to know when it is your turn.

There is also often a flow that develops, or a theme which becomes apparent as the meeting progresses. Christian believers have the Spirit of God and know his voice (John 10:27; see Rom. 8:1-14). During the meeting, individuals may sense the Spirit of God nudging them to participate. Others may have simply prepared something in advance and choose a reasonable time to share it. This may all sound mysterious, and it might be, but when you begin to gain experience with it, it becomes very natural. 

If you have the opportunity to attend a gathering like this, you might want to listen and observe for awhile. But if you are a Christian believer, you would also be welcome to jump right in. 

## Why would we want to do this?

If you are a Christian believer, then Jesus has invited you to his table. He welcomes all of his people — his body, his children, his family — to participate freely in worshiping him.

Many churches have good teaching and excellent programs, but no opportunity for Christian believers to share with each other their offerings of worship to Jesus in an open meeting around the Lord’s table. Many Christian communities have excellent, skilled leaders, but no opportunity for the rest to speak with the Lord and each other as they meet together. 

Where else have you had the opportunity to worship Christ in this way? It might be that this kind of worship meeting is what you have been missing.

## What if someone messes up or says something ridiculous?

Yes, that happens. And? We’re all human. Jesus accepts us as we are, listens to our fumbling attempts to worship him, and welcomes us at his table. Since that is how he treats each of us, we should treat one another the same way.

With great freedom comes great responsibility. Each one has a responsibility to bring good gifts to the table (1 Cor. 11:27-32; Matt. 5:23-24), to seek to follow the leading of God’s Spirit, and to resist the flesh (1 Cor. 11:18-22). As each one speaks, the others have the duty to listen and to evaluate what is said (1 Cor. 14:29). 

This way of celebrating the table of Jesus both requires and cultivates maturity, discernment, and authenticity.

Once in awhile, someone will attend a open meeting like the table of Jesus and make it into an opportunity for self-aggrandizement, grandstanding, or getting on their soapbox. It is important to recognize that we are in the presence of Jesus, we are at his table, and that this kind of behavior, while natural for humans, is inappropriate at the Lord’s table (1 Cor. 11:27-32). 

Nevertheless, many of us have done something like that at some point. Jesus accepts us as we are and is patient with us in our ridiculousness. Usually the best approach is just to be patient with the other person in the moment and then look for an opportunity to talk with them in a constructive way later. Very rarely, it is necessary to interrupt someone during the meeting and ask them to stop speaking or even to leave if they become hostile and abusive. 

These occasions may cause us to grieve, but they affirm to us the value of meeting in this way: It brings out what really is in people’s hearts and lives, and it gives the whole community the opportunity to grow together as we present ourselves, just as we are, to the Lord.

Because issues do arise, it is important for every group who meets in this way to have one or more recognized authorities oversee the meeting — to make sure things stay on track and provide a steady vision, guidance, and gentle correction. When the group is both male and female, it is our conviction that these authoritative “overseers” should be men (1 Tim. 2:12).

## About that: Do you treat women as equal to men?

On the one hand, there are Christian believers who are convinced that women as well as men should be authoritative overseers in the body of Christ. We respect those who believe this, but it does not seem to us to be possible to say this without setting aside some of the teaching of Scripture. Such passages as 1 Tim. 2:11-14 and 1 Cor. 14:34-35 convince us that authoritative teachers and overseers in the Christian community should be men.

On the other hand, there are Christian believers who are convinced that women should not participate verbally in meetings like this, usually on the basis of 1 Cor. 14:34-35. However, earlier in the same extended passage about the Lord’s supper, women are clearly portrayed as praying and prophesying (1 Cor. 11:5), and also elsewhere (Acts 21:9). It is a hallmark of the coming of the Spirit and the new covenant community that both men and women, both young and old, have direct access to the Spirit of God and proclaim his word (Acts 2:17-18).

The key to reconciling these passages is to understand that (a) worshiping aloud does not constitute an exercise of authority, but simply of worship, and (b) Paul in 1 Cor. 14:34-35 was not prohibiting all speech by women in these meetings, but only disruptive speech (asking questions, for example, in a way that distracts from worship). On this question each community should both be fully convinced, and yield to the decision of those in authority.

## Do people need to leave their current church?

The table of Jesus is open to all Christian believers. It is a meeting, not a church _per se_. However, groups of Christians who are meeting in this way would naturally form a new church in time.

## This sounds really good. Does it really work?

Yes, it really works. There are groups of Christians who have been gathering in these ways for decades. It requires authenticity and maturity, but those who worship in this way tend to develop those traits. We believe that worshiping this way is one of the best ways for Christians to develop real maturity as Christ’s followers and authenticity as a community of his people.

## I’d like to try this. What do I need to do?

You need two or three Christian believers who want to meet with Jesus in this way (Matt. 18:19-20). You need a place and time to meet, and some kind of bread/cracker and wine/juice. Most important, you need to have a willingness to follow the Lord’s Spirit and “become subject to one another out of reverence for Christ” (Eph. 5:21).

There are several passages of Scripture that speak about this way of meeting and give instructions for it. It would be good for those who are participating in these meetings to study 1 Corinthians 11 and 14, Romans 12, and 1 Timothy 2.

When you meet together, make it your aim to follow the Lord’s leading. You will find him leading your meetings in the most delightful and surprising ways.

## Passages that Speak to This Practice

### Acts 2:42

They devoted themselves to the apostles’ teaching and to fellowship, to the breaking of bread and to prayer. —Acts 2:42, NIV

### 1 Corinthians 11:23-26

For I received from the Lord what I also passed on to you: The Lord Jesus, on the night he was betrayed, took bread, and when he had given thanks, he broke it and said, “This is my body, which is for you; do this in remembrance of me.” In the same way, after supper he took the cup, saying, “This cup is the new covenant in my blood; do this, whenever you drink it, in remembrance of me.” For whenever you eat this bread and drink this cup, you proclaim the Lord’s death until he comes. —1 Cor. 11:23-26, NIV

### 1 Corinthians 14:26

When you meet together, one will sing, another will teach, another will tell some special revelation God has given, one will speak in tongues, and another will interpret what is said. But everything that is done must strengthen all of you. —1 Cor. 14:26, NLT

### 1 Corinthians 14:29-31

Two or three prophets should speak, and the others should weigh carefully what is said. And if a revelation comes to someone who is sitting down, the first speaker should stop. For you can all prophesy in turn so that everyone may be instructed and encouraged. —1 Cor. 14:29-31, NIV

### Ephesians 5:21

Submit to one another out of reverence for Christ. —Eph. 5:21, NIV

### Matthew 18:20

“For where two or three are gathered in my name, there am I among them.” —Matt. 18:20, ESV

## Definitions

### The Good News of Jesus

When Jesus began his public ministry around the age of 30, he began proclaiming the message, “Repent, for the kingdom of heaven is at hand” (Matt. 4:17). In other words, the time had come to repent — to turn away from sin and turn to God — because the kingdom of heaven — the reality of God’s rule over all things — was now at hand. Jesus was the herald of the coming kingdom of God.

But the herald was also the king. God’s reign was beginning through Jesus himself, through his presence in the world and through the work that he did. That is why he healed the sick, raised the dead, and preached justice and mercy and peace: Jesus himself brought about the reality of God’s rule. 

The most important thing that Jesus did during his life was to die and rise from the dead. Human beings are, by nature, alienated from God and under judgment for their sin. God’s justice requires that every evil committed in the hearts and minds and actions of human beings be dealt with, because every evil has the capacity to corrupt us completely. But we have all committed many evils, and these things make it impossible for us to stand in the holy presence of God. When Jesus died, he took on himself the judgment that all sins deserve. When he rose from the dead, he demonstrated his victory over the corruption of sin and death. So God’s justice has been satisfied without our having to experience his judgment. We can experience his forgiveness and life by putting our trust solely in Jesus Christ as our Lord and Savior. That is, in a nutshell, the good news of Jesus.

### Christian Believers

Christian believers are simply people who have put their trust solely in Jesus Christ as their Lord and Savior. They believe the good news of Jesus. They trust Jesus to rescue them from their sin, to reconcile them to God, and to reward them with eternal life. And they follow him, trusting him as their Lord.

_—Sean Harrison &lt;[sah.harrison@gmail.com](mailto:sah.harrison@gmail.com)&gt;, © 2016, 2018 [CC-BY-SA](https://creativecommons.org/licenses/by-sa/4.0/)_

