
+ 0.3.0 (2018-12-02)

  + Removed section: Do we need to get permission from someone to meet like this?
  + light revision
  + plain .html output from Typora
+ 0.2.1 (2018-08-18)
  - remove stray anchors (conversion artifacts)
+ 0.2.0 (2018-07-19)
  - slight revision, particularly the sections having to do with the roles of women.
  - published for review among friends.
+ 0.1.0 (2016-04-23) 
  - first version published 2016 (sources not available here).
